Atmel MCU code examples
=======================

Code that I write experimenting with Atmel MCUs, mostly in Assembler and C.
All sources are compiled with avr-gcc and I try to stick to the [ABI](https://gcc.gnu.org/wiki/avr-gcc#ABI).
