#ifndef     ATTINY2313_PRESCALERS
#define     ATTINY2313_PRESCALERS   1

#define     PRESC_1     0x01
#define     PRESC_8     0x02
#define     PRESC_64    0x03
#define     PRESC_256   0x04
#define     PRESC_1024  0x05

#endif
