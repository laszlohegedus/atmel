/****h* blink/blink
 *
 * NAME
 *  blink -- An LED blinking program that uses Timer0 overflow interrupt
 *
 * NOTES
 *  Tested with an Attiny2313 using an internal clock set to 4MHz.
 *  A prescaler of 1/1024 is used, thus the LED is toggled ~15 times per
 *  second. The MCU used for testing was slightly faster than 4MHz, it
 *  toggled the LED ~18 times per second.
 *
 * AUTHOR
 *  Laszlo Hegedus
 *
 ******
 */
        .section .data
#define     __AVR_ATtiny2313__  1

#include "/usr/lib/avr/include/avr/io.h"
#include "/usr/lib/avr/include/avr/sfr_defs.h"

; Use pin 6 of Port D
#define     io(port)        _SFR_IO_ADDR(port)
#define     LED             PORTD6
#define     DDR             io(DDRD)
#define     PORT            io(PORTD)

#define     PRESC_1024  ((1 << CS02) | (1 << CS00))

;; Timer0 overflow interrupt vector

#define     toggle_led  TIMER0_OVF_vect

        .section .text

        .global main

/****f* blink/main
 *
 * NAME
 *  main -- The intented entry point of this program
 *
 * FUNCTION
 *  Initializes the environment for LED blinking,
 *  then enters into an infinite loop.
 *
 ******
 */
main:
        rcall   init
1:
        nop
        rjmp    1b

/****f* blink/init
 *
 * NAME
 *  init -- Initializes the MCU
 *
 * FUNCTION
 *  Sets the specified pin for output.
 *  Turns on Timer0 overflow interrupt.
 *  Sets the 1/1024 prescaler for Timer0.
 *
 ******
 */
init:
        sbi     DDR,        LED         ; Set LED pin as output

        ldi     r18,        PRESC_1024  ; Set prescaler to 1/1024
        out     io(TCCR0B), r18         ; for Timer0

        ldi     r18,        (1 << TOIE0); Enable Timer0 overflow interrupt
        out     io(TIMSK),  r18         ; in the Timer Interrupt Mask.

        sei                             ; Set global interrupt flag
        ret

        .global toggle_led

/****f* blink/toggle_led
 *
 * NAME
 *  toggle_led -- toggles the state of the LED
 *
 * FUNCTION
 *  Depending on whether the LED is on or off it is turned off or on,
 *  respectively.
 *  The output of the given port is pulled up or down.
 *
 ******
 * This function is called automatically in case of a Timer0 overflow
 * interrupt.
 *
 */
toggle_led:
        sbis    PORT,   LED
        rjmp    turn_on
        rjmp    turn_off
    turn_on:
        sbi     PORT,   LED
        reti
    turn_off:
        cbi     PORT,   LED
        reti
