#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

#define LED PD6


void toggle_led();
void setup();

ISR(TIMER1_OVF_vect)
{
    toggle_led();
}


void toggle_led() // Toggle led and put cpu to sleep
{
    PORTD ^= (1 << LED);
    sleep_enable();
    ACSR |= (1 << ACD); // Disable analog comparator
    sei();
    sleep_cpu();
    sleep_disable();
}


void setup(void)
{
    DDRD |= (1 << LED); // PIND6 is output
    PORTD |= (1 << LED); // Turn it on

    TIMSK |= 0x80; // Set TOIE1 bit, enable TIMER1 overflow

    // Set 64 as divisor, overflow in roughly one second
    TCCR1B |= (1 << CS11) | (1 << CS10);
    TCCR1B &= ~(1 << CS12);
}


int main(void)
{
    setup();
    sei();
    while (1)
    {
    }
}
