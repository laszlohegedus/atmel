/****h* 7slcd/7slcd
 *
 * NAME
 *  7slcd -- Header file for using an 7 segment LCD display with an AVR MCU
 *
 * AUTHOR
 *  Laszlo Hegedus
 *
 ******
 */

#ifndef     __7SLCD__
#define     __7SLCD__ 1


/****f* 7slcd/display_digit
 *
 * NAME
 *  display_digit -- Display a digit on the LCD screen
 *
 * INPUTS
 *  digit   - An 8 bit unsigned integer. Should be between 0 and 9.
 *
 * FUNCTION
 *  Displays the given digit on the LCD screen.
 *
 ******
 */
void display_digit(uint8_t digit);

#endif
